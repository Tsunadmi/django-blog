from django.shortcuts import render, get_object_or_404
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, PasswordChangeForm
from .form import SingUpForm, EditProfilForm, PasswordChangingForm, ProfilePageform
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.views import PasswordChangeView
from django.views.generic import DetailView, CreateView
from shop.models import Profile


class CreateProfilePage(CreateView):
    model = Profile
    template_name = 'registration/create_user_profile.html'
    form_class = ProfilePageform

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class EditProfilePage(generic.UpdateView):
    model = Profile
    template_name = 'registration/edit_user.html'
    fields = ['bio', 'profile_picture', 'linkedin_url', 'gitlab_url', 'insta', 'facebook']
    success_url = reverse_lazy('login')


class ShowProfilePage(DetailView):
    model = Profile
    template_name = 'registration/user.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ShowProfilePage, self).get_context_data(*args, **kwargs)
        user_list = get_object_or_404(Profile, id=self.kwargs['pk'])
        context['user_list'] = user_list
        return context


class PasswordChange(PasswordChangeView):
    success_url = reverse_lazy('password_success')
    form_class = PasswordChangingForm


def password_success(request):
    return render(request, 'registration/password_success.html', {})


class UserRegisterView(generic.CreateView):
    form_class = SingUpForm
    template_name = 'registration/register.html'
    success_url = reverse_lazy('login')


class UserEditView(generic.UpdateView):
    form_class = EditProfilForm
    template_name = 'registration/edit_profile.html'
    success_url = reverse_lazy('home')

    def get_object(self):
        return self.request.user
