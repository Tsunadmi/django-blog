from django.contrib.auth import views as auth_views
from django.urls import path, include
from .views import UserRegisterView, UserEditView, PasswordChange, ShowProfilePage, EditProfilePage, CreateProfilePage
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
                  path('register/', UserRegisterView.as_view(), name='register'),
                  path('edit/', UserEditView.as_view(), name='edit_profile'),
                  path('password/', PasswordChange.as_view(template_name='registration/change-password.html')),
                  path('password_success/', views.password_success, name='password_success'),
                  path('<int:pk>/profile', ShowProfilePage.as_view(), name='user_profile'),
                  path('<int:pk>/profile_edit', EditProfilePage.as_view(), name='user_profile_edit'),
                  path('profile_create', CreateProfilePage.as_view(), name='user_profile_create'),
              ]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + staticfiles_urlpatterns()
