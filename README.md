# Django Blog

Django project

## Installation

You need install python 3.9:
```
pip install python
```
## Virtual environment

In terminal in project folder you should activate virtual environment:

```
venv/Scripts/activate
```

## Add important libraries

```
asgiref==3.5.2
Django==4.0.6
django-ckeditor==6.5.0
django-js-asset==2.0.0
Pillow==9.2.0
sqlparse==0.4.2
tzdata==2022.1

```

## Getting started

Start BLOG:
```
python manage.py runserver
```
## Technologies used
 
- Python
- JetBrains
- HTML
- CSS
- Django

## Description

Project, where you can register and create your blog, also you can follow blog other users.

Your possibility:
- Create/Edit/Delete post.
- Create/Edit/Delete comments under each post.
- Put like/Dislike under each post.
- Create/Edit/Delete category of post.
- Upload image.
- Create/Edit/Delete our profile.

- I will update the next feature of this post in the future.

All detailed information you can find in the code above.

## Author

This app was done by Dmitry Tsunaev.

- [ ] [LinkedIn](http://linkedin.com/in/dmitry-tsunaev-530006aa)
