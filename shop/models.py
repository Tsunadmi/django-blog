from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from ckeditor.fields import RichTextField


class Category(models.Model):
    name =  models.CharField(max_length=128)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('home')

class Profile(models.Model):
    user = models.OneToOneField(User, null= True, on_delete=models.CASCADE)
    bio = models.TextField()
    profile_picture = models.ImageField(null=True, blank=True, upload_to='images/profile')
    linkedin_url = models.CharField(max_length=64, blank=True, null=True)
    gitlab_url = models.CharField(max_length=64, blank=True, null=True)
    insta = models.CharField(max_length=64, blank=True, null=True)
    facebook = models.CharField(max_length=64, blank=True, null=True)
    title = models.CharField(max_length=64, blank=True, null=True)

    def __str__(self):
        return str(self.user)

    def get_absolute_url(self):
        return reverse('home')

class Post(models.Model):
    title = models.CharField(max_length=64)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    body = RichTextField(blank=True, null=True )
    title_tag = models.CharField(max_length=64, default='Blog page')
    date = models.DateField(auto_now_add=True)
    category = models.CharField(max_length=128, default='coding')
    likes = models.ManyToManyField(User, related_name='blog_post')
    header_image =models.ImageField(null=True, blank=True, upload_to='images/')

    def total_like(self):
        return self.likes.count()

    def __str__(self):
        return self.title + ' | ' + str(self.author)

    def get_absolute_url(self):
        # return reverse('detail', args=(str(self.id)))
        return reverse('home')

class Comments(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.text},  {self.created_date}, {self.user}'
