from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Post, Category, Comments
from .forms import PostForm, CommentForm
from django.http import HttpResponseRedirect


def LikeView(request, pk):
    post = get_object_or_404(Post, id=request.POST.get('post_id'))
    liked = False
    if post.likes.filter(id=request.user.id).exists():
        post.likes.remove(request.user)
        liked = False
    else:
        post.likes.add(request.user)
        liked = True
    return HttpResponseRedirect(reverse('detail', args=[str(pk)]))


class HomeView(ListView):
    model = Post
    template_name = 'home.html'
    ordering = ['-date']

    def get_context_data(self, *args, **kwargs):
        list_menu = Category.objects.all()
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        context['list_menu'] = list_menu
        return context


def CategoryView(request, list):
    category_list = Post.objects.filter(category=list.replace('-', ' '))
    return render(request, 'category.html', {'list': list.title().replace('-', ' '), 'category_list': category_list})


class ArtDetailView(DetailView):
    model = Post
    template_name = 'post_details.html'

    def get_context_data(self, *args, **kwargs):
        staff = get_object_or_404(Post, id=self.kwargs['pk'])
        total_likes = staff.total_like()
        liked = False
        if staff.likes.filter(id=self.request.user.id).exists():
            liked = True
        context = super(ArtDetailView, self).get_context_data()
        context['total_likes'] = total_likes
        context['liked'] = liked
        return context


class AddNewPost(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'add_post.html'
    # fields = '__all__'
    # fields = ('title','body')


class AddNewCategory(CreateView):
    model = Category
    template_name = 'add_category.html'
    fields = '__all__'



class EditPost(UpdateView):
    model = Post
    template_name = 'add_post.html'
    fields = ['title', 'title_tag', 'body']


class DeletePost(DeleteView):
    model = Post
    template_name = 'delete_post.html'
    success_url = reverse_lazy('home')


def post_comment_new(request, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.post = post
            comment.save()
        return redirect('detail', post_pk)
    else:
        form = CommentForm()
        context = {"form": form}
        return render(request, "add_comment.html", context)


def comment_delete(request, comment_pk, post_pk):
    comments = Comments.objects.filter(id=comment_pk)
    comments.delete()
    return redirect('detail', post_pk)


def comment_edit(request, comment_pk, post_pk):
    post = get_object_or_404(Post, pk=post_pk)
    comment = get_object_or_404(Comments, id=comment_pk)
    if request.method == 'POST':
        form = CommentForm(request.POST, instance=comment)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.user = request.user
            comment.post = post
            comment.save()
        return redirect('detail', post_pk)
    else:
        form = CommentForm(instance=comment)
        context = {"form": form}
        return render(request, "add_comment.html", context)
