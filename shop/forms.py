from django import forms
from .models import Post, Category, Comments

choises = Category.objects.all().values_list('name', 'name')
choises_list = []
for item in choises:
    choises_list.append(item)


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comments
        fields =('text',)

        widgets = {
            'text': forms.Textarea(attrs={'class': 'form-control'}),
        }

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'title_tag', 'author', 'body', 'category', 'header_image')

        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'title_tag': forms.TextInput(attrs={'class': 'form-control'}),
            'author': forms.TextInput(attrs={'class': 'form-control', 'value': '', 'id': 'elder', 'type': 'hidden'}),
            'category': forms.Select(choices=choises_list, attrs={'class': 'form-control'}),
            'body': forms.Textarea(attrs={'class': 'form-control'}),

        }
