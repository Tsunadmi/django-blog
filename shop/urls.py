
from django.urls import path, include
from .views import HomeView, ArtDetailView, AddNewPost, EditPost, DeletePost, AddNewCategory, CategoryView, LikeView, post_comment_new, comment_delete, comment_edit
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('', HomeView.as_view(), name = 'home'),
    path('detail/<int:pk>', ArtDetailView.as_view(), name = 'detail'),
    path('add_post/', AddNewPost.as_view(), name = 'add_post'),
    path('detail/edit/<int:pk>', EditPost.as_view(), name = 'edit_post'),
    path('detail/<int:pk>/delete', DeletePost.as_view(), name = 'delete_post'),
    path('add_category/', AddNewCategory.as_view(), name = 'add_category'),
    path('category/<str:list>/', CategoryView, name = 'category'),
    path('like/<int:pk>/', LikeView, name = 'like_post'),
    path('detail/<int:post_pk>/comment/', post_comment_new, name = 'add_comment'),
    path('detail/edit/<int:post_pk>/<int:comment_pk>', comment_edit, name='edit_comment'),
    path('comment/delete/<int:post_pk>/<int:comment_pk>', comment_delete, name='delete_comment'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + staticfiles_urlpatterns()